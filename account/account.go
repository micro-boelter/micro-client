package account

import (
	"context"
	"errors"
	"fmt"
	"time"

	pb "gitee.com/micro-boelter/micro-client/proto-buffer"
	"google.golang.org/grpc"
)

var (
	address   = ""
	appkey    = ""
	appsecret = ""
)

func Init(addr string, key string, secret string) {
	address = addr
	appkey = key
	appsecret = secret
}

func getClient() (*grpc.ClientConn, error) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, fmt.Errorf("did not connect: %v", err)
	}
	return conn, nil
}

func Register(username string, password string, redirect string) (*pb.CommonReply, error) {
	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.Register(ctx, &pb.RegisterRequest{Username: username, Password: password, Redirect: redirect})
}

func CreateApp(name string) (*pb.CreateAppReply, error) {
	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.Addapp(ctx, &pb.CreateAppRequest{Name: name})
}

func Login(name string, psw string, redirect string) (*pb.CommonReply, error) {
	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.Login(ctx, &pb.LoginRequest{Username: name, Password: psw, Appkey: appkey, Appsecret: appsecret, Appredirect: redirect})
}

func GetUserInfo(token string) (*pb.GetInfoReply, error) {
	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.Getuserinfo(ctx, &pb.GetInfoRequest{Token: token})
}

func ValidUrl(url string) (*pb.ValidReply, error) {
	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.Validurl(ctx, &pb.ValidRequest{Url: url})
}

func ValidApp(key string, secret string) (*pb.ValidReply, error) {
	if len(key) == 0 {
		key = appkey
	}
	if len(secret) == 0 {
		secret = appsecret
	}
	if len(key) == 0 || len(secret) == 0 {
		return nil, errors.New("参数错误")
	}

	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.Validapp(ctx, &pb.ValidAppRequest{Appkey: key, Appsecret: secret})
}

func ValidEx(appredirect string) (*pb.ValidReply, error) {
	if len(appkey) == 0 || len(appsecret) == 0 {
		return nil, errors.New("参数错误")
	}

	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewUserCenterClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	return c.ValidEx(ctx, &pb.ValidExRequest{Appkey: appkey, Appsecret: appsecret, Appredirect: appredirect})
}
