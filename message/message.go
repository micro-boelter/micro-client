package message

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/micro-boelter/micro-client/proto-buffer"
	"google.golang.org/grpc"
)

var (
	address   = ""
	appkey    = ""
	appsecret = ""
)

func Init(addr string, key string, secret string) {
	address = addr
	appkey = key
	appsecret = secret
}

func getClient() (*grpc.ClientConn, error) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, fmt.Errorf("did not connect: %v", err)
	}
	return conn, nil
}

func SendMsg(target string, msg string) (*pb.CommonMsgReply, error) {
	conn, err := getClient()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	c := pb.NewMessageSvrClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	return c.Sendmsg(ctx, &pb.MessageRequest{From: target, Msg: msg, Appkey: appkey, Appsecret: appsecret})
}
